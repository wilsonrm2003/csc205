Introduction:
    
    - The ALTAIR 8800 was the first computer easily affordable
    to the public
    
    - the ALTAIR is run by Intel Corporation's Model 8080 Microcomputer
    a CPU on a single chip
    
    - The Intel Mircocomputer chip represented a major technological
     breakthrough
    
    - The operating manual was made for both the experienced and 
    unexperienced user
    
    - The computer has 78 machine languages and is able to preform 
    many operations mini computers cannot
    
    - after reading the manual novices will be able to load programs
    on the ALTAIR 
    
    - Users with any type of electronic background with electronics will
    be able to use the ALTAIR as the user manual was written with all 
    backgrounds in mind

    - What Each Part of the Manual will go over:
        Part One: 
        
        - prepares the user for better understanding of computer terminology,
        technology, and operation
        
        - introduces conventional and electronic logic 

        - a description of important number systems 
        
        - a discussion of basic programming and computer languages  
        
        Part Two and Three:
        
        - describe organization and operation of the ALTAIR 
        
        - Empahsis on the portions most frequently used by the computer 
        
        Part Four:
        
        - detailed listing of the 78 ALTAIR's 78 Instructions
        
        - Has an Appendix which condenses the instructions into a quick
        reference list
        
    - Carefully reading the manual will fully prepair you for operating
    the ALTAIR 8800
    
    - as you gain experience using the machine you will understand its 
    vercatility and processing ability
    
    - Don't be scared if the manual is complicated at times remember the
    computer only does what the programmer tells it to
    
A. Logic:
    
    - George Boole:
        - showed how any logical statement can be analyzed with 
        simple arithmetic relationships
        - in 1847 he published a book called The Mathematical Analysis
        of Logic
        - in 1854 published a much more detailed work on the subject 
        -"to this day" all practical digital computers and many 
        other electronic circuits are based on the logic concepts explained
        by Boole
    
    - Boole's system of logic is frequently called Boolean algebra
    assumes that a logic condition or station is true or false
    
    - electronic circuits are suited for this kind of operation
   
    - if a circuit in the ON state is said to be true and OFF is false 
    
    - using this method an electronic analogy of a logical statement can 
    be readily synthesized

    - with these you can make three electronic equivalents for the three basic
    logic statements AND, OR, and NOT
    
    - The AND statement is true if anf only if either or all of its 
    logic conditions are true
    
    - A NOT statement reverses the meaning of a logic statement so 
    that a true statement is false and a false statement is true 

    - its easy to generate a simple equivalent of these three logic statements 
    by using on-off switches

    - a switch is ON is said to be true and a switch that is OFF is false

    - Since a switch is OFF will not pass an electrical current 
    it can be assigned a numerical value of 0 similarly an 
    ON switch can be assigned 1 

    - Looking at the per mutations for a two condition AND statement 
    we can see that only if both condtitons are True then the conclusion
    is True anything else makes the conclusion false

    - using an OR statement we can see that as long as both conditions are 
    not OFF/0 then the conclusion is true 

    - using a NOT statement we can see that the conclusion is only true 
    if the condition is false 

B. Electronic Logic:

    - All three of the basic logic functions can be implemented by relatively   
    simple transistor circuits 
    
    - by convention each circuit is assigned a symbol to assist in 
    designing logic systems

    - the AND statement is showed by |)

    - the OR statement is showed by ))

    - the NOT statement is showed by a triangle
    
    - the three basic circuits can be combined with eachother to produce more
    logic statements
    
    - two of them are used so often they are considered basic logic
    circuits and have their own symbol

    - The NAND is for (NOT-AND) and the conclusion is only true if 
    the conditions are true 

    - The NOR is for (NOT-OR) and the conclusion is only true if none of the 
    conditions are true

    - Three or more logic circuits make a logic system and one of the most 
    basic logic systems is the EXCLUSIVE-OR circuit

    - The EXCLUSIVE-OR circuit can be used to implement logical functions 
    and it can add two input conditions 

    - Since electronic logic circuits utilize two numerical units 0 and 1 
    they are compatible with the binary number system, a number system 
    which has only two digits for this reason EXCLUSIVE-OR circuit is often 
    called a binary adder

    - the EXCLUSIVE-OR (XOR) is represented as ) ))
    
    - Various combinations of logic circuits can be used to implement numerous 
    electronic functions, like two NAND circuits can be connected to form 
    a bistable circuit called a flip flop

    - Since the Flip-flop changes state only when an incoming signal
    in the form of a pulse arrives it acts as a short term memory element
    
    - several flip-flops can be cascaded together to from 
    electronic counters and memory registers 
    
    - other logic circuits can be connected together to form monostable 
    and astable circuits
    
    - monostable circuits occupy one of two states unless an incoming 
    pulse is received
    
    - they then occupy an opposite state for a brief time and then resume their
    normal state
    
    - Astatble circuits continually switch back and forth between two states 

C. Number Systems:

    - early humans devised a number system which consisted of ten digits
    
    - number systems can be based on any number of digits 
    
    - as we have seen dual state electronic circuits are highly compatible
    with two digit number systems and its digits are termed "bits" 

    - Systems based on eight and sixteen are also compatible with complex 
    electronic logic systems such as computers since they provide a
     convenient shorthand method for expressing long binary numbers 

D. The Binary System:

    - the ALTAIR 8800 performs nearly all operations in binary

    - a typical binary number processed by the computer incorporates 8-bits 
    and may appear as: 10111010
    
    - a fixed length binary number such as this is usually called a word or 
    byte and computers are usually designed to process and store a fixed 
    number or bytes 

    - a binary word like 10111010 appears totally meaningless to the novice  

    - but since binary uses only two bits it is actually much simpler 
    than the familiar and traditional decimal system
    
    - to see why let's derive the binary equivalents for the 
    decimal numbers from 0 to 20 
    
    - we will do this by adding 1 to earch successive number until all the 
    numbers have been derived 

    - counting in any number system is governed by one basic rule: 
    record successive digits for each ount in a column 
    
    - when the total number of available digits has been used begin a new 
    column to the left of the first and resume counting 

    - counting from 0 to 20 in binary is very easy since there are only 
    two digits 
    
    - the binary equivalent of the decimal 0 is 0 and the decimal 1 is 1 
    
    - since both available bits have been used the binary count 
    must incorporate a new column to for the binary equivalent 2 which is 10. 
    the binary 3 is 11 both bits are used again so we need a third column 
    to get 4 (100) 
    
    - Binary 0 to 20: (decimal | binary)
        * 0 | 0
        * 1 | 1 
        * 2 | 10 
        * 3 | 11 
        * 4 | 100
        * 5 | 101
        * 6 | 110 
        * 7 | 111
        * 8 | 1000
        * 9 | 1001
        * 10| 1010
        * 11| 1011
        * 12| 1100
        * 13| 1101
        * 14| 1110
        * 15| 1111
        * 16| 10000
        * 17| 10001
        * 18| 10010
        * 19| 10011
        * 20| 10100
    
    - a simple procedure can be used to convert a binary number to the 
    decimal equivalent
    
    - each bit in a binary number indicated by which power of two the number
     to be raised
    
    - the sum of the powers of two gives the decimal equivalent for the number
