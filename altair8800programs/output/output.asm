	PAGE	40	; 40 lines per page
	TITLE	"Testing output program - Rachael"
; Code Segement 

	INCLUDE stdlib	; Include standard I/O library 

	ORG	000h	; Load at memory location 000 (hex)
	
	CALL RSTIO	; Initialize serial input / output bus
	CALL WRITE	; Write WORDS to stdout

HCF:	HLT		; Halt
	
; Data Segment

	ORG	0200h	; Load at memory location 200 (hex)

WORDS:	DB	"Hello! ;)"
	DB	CR,LF	; Old-school CRLF newline
	DB	NUL	; NULL string terminator

	END
